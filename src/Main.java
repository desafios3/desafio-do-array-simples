import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        Metodo metodo = new Metodo();
        Scanner entrada = new Scanner(System.in);
        String nome, nomes;
        int opcao;
        boolean sair = false;

        do {
            metodo.exibir();
            System.out.println("\n1. Adicionar cotacao");
            System.out.println("2. Remover cotacao");
            System.out.println("3. Alterar o titulo de uma das cotacoes");
            System.out.println("4. Sair");
            System.out.print("Escolha sua opção: ");
            opcao = entrada.nextInt();

            switch (opcao) {
                case 1:
                    entrada.nextLine();
                    System.out.print("Titulo: ");
                    nome = entrada.nextLine();
                    metodo.cadastrar(nome);
                    System.out.println("\n" + nome + " adicionado com sucesso!\n");
                    break;

                case 2:
                    entrada.nextLine();
                    System.out.println("Informe o titulo a ser removido: ");
                    metodo.remover(entrada.next());

                    break;

                case 3:
                    entrada.nextLine();
                    System.out.println("Informe o titulo a ser alterado ");
                    nome = entrada.nextLine();
                    System.out.println("Informe o titulo atualizado: ");
                    nomes = entrada.nextLine();
                    metodo.altera(nome, nomes);
                    System.out.println("Titulo atualizado com sucesso! ");

                    break;

                case 4:

                    sair = true;
                    System.out.println("\nAté logo!\n");
                    break;
                default:
                    System.out.println("\nOpção inválida!\n");
            }
        }
        while (!sair);

        entrada.close();
    }
}
